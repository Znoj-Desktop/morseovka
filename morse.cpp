#include <iostream>
#include <string>
#include "ctype.h" //pro zkoumani znaku

using namespace std;

const int fileLenght = 20;
//tabulky pro prevod do morseovy abecedy
const string alpha[26] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-",
	".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
const string number[10] = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};
//tabulka pro prevod diakritiky
const int iso8859_2[102] = {83, 0, 83, 84, 90, 90, 0, 65, 0, 0, 0, 76, 83, 0, 0, 83, 83, 84, 90, 0, 
	90, 90, 0, 65, 0, 0, 0, 76, 83, 0, 0, 83, 83, 84, 90, 0, 90, 90, 82, 65, 65, 65, 65, 76, 67, 67,
	67, 69, 69, 69, 69, 73, 73, 68, 68, 78, 78, 79, 79, 79, 79, 0, 82, 85, 85, 85, 85, 89, 84, 0, 82,
	65, 65, 65, 65, 76, 67, 67, 67, 69, 69, 69, 69, 73, 73, 68, 68, 78, 78, 79, 79, 79, 79, 0, 82, 85,
	85, 85, 85, 89, 84, 0};
//funkce vypise text napovedy
void help(){
	cout << "h - napoveda programu" << endl;
	cout << "0 - z textoveho souboru do morseovky" << endl;
	cout << "1 - z morseovky do textoveho souboru" << endl;
	cout << "k - konec programu "<< endl;
	cout << "autor: Iri" << endl;
	cout << "soubory by mely byt v kodovani ISO 8859-2, nebo CP-1250" << endl;
}

FILE * openFile(string type){
	//nacteni nazvu souboru od uzivatele
	char fileName[fileLenght];
	cout << "Zadejte nazev "<< type << "niho souboru s cestou: " << endl;
	cin >> fileName;

	FILE * file;
	char *mode = "r";
	//pokud je soubor vystupni, nastavi se priznak pro zapis
	if(type.compare("vystup") == 0){
		mode = "w";
	}
	
	//otevreni a kontrola souboru
	if((file = fopen(fileName, mode)) == NULL){
		cout << "Chyba pri otevirani "<< type << "niho soubrou" << endl;
	}
	return file;
}

//v ascii tabulce jsou cisla od cisla 48 (=0) po cislo 57 (=9)
//v ascii tabulce jsou pismena od cisla 65 (=A) po cislo 90 (Z)
//v ascii tabulce jsou pismena od cisla 97 (=a) po cislo 122 (z)
string toMorse(int numberAscii){
	//cisla
	if(numberAscii < 58){
		return number[numberAscii-48];
	}
	//velka pismena
	else if(numberAscii < 91){
		return alpha[numberAscii-65];
	}
	//mala pismena (numberAscii < 123)
	else{
		return alpha[numberAscii-97];
	}
}

//OSETRIT!!!
char toString(string morse){
	//cisla
	if(morse.length() > 5){
		return EOF; //spatne zapsan znak - nepatri do morseovy abecedy
	}

	else if(morse.length() == 5){
		for(int i=0; i<10; i++){
			if(morse.compare(number[i]) == 0){
				return (char) (i+48);
			}
		}
		return EOF; //spatne zapsan znak - nepatri do morseovy abecedy
	}

	//pismena
	else{
		for(int i=0; i<26; i++){
			if(morse.compare(alpha[i]) == 0){
				return (char) (i+65);
			}
		}
		return EOF; //spatne zapsan znak - nepatri do morseovy abecedy
	}
}


//funkce pro prepis z textu do morseovky
bool prepis0(FILE * infile,FILE * outfile){
	int numberAscii;
	char character;
	//podle predchoziho znaku se urcuje pocet lomitek
	string previous;
	previous.clear(); //vymazani obsahu stringu
	const char *buffer;
	string morse;
	numberAscii = fgetc(infile); //osetrit prazdny soubor???
	do{
		if(numberAscii>153){
			//prevod z diakritiky
			numberAscii = iso8859_2[numberAscii-154];
		}
		else if(numberAscii>127){
			cout << "...Text nelze prevezt do morseovy abecedy..." << endl;
			return 1;
		}
		//prevod z cisla udavajici hodnotu znaku v ascii tabulce na znak
		character = (char) numberAscii;
		//pokud je znak cislo/pismeno
		if(isalnum(character)){
			//funkce pro prevod textu do morseovky
			morse = toMorse(numberAscii);
			//na konci pismena vzdy lomitko
			cout << morse << "/";
			//je treba pouzit buffer, abychom mohli zapsat string do souboru, 
			//ktery je otevren pomoci fopen
			buffer = morse.c_str();
			//zapis
			fputs(buffer, outfile);
			fputs("/", outfile);
			//vse co bylo v previous se nahradi jednim lomitkem
			previous = "/";
		}
		else if(character == ' '){
			//jen na konci slova a vzdy pouze 2 lomitka at je mezer kolik chce...
			if(previous.compare("/") == 0){
				cout << "/";
				fputs("/", outfile);
				previous += "/";
			}
		}
		else if(character == '\n'){
			if(previous.compare("\n") != 0){
				cout << endl;
				fputs("\n", outfile);
				previous += "/";
			}
			previous = "\n";
			
		}
		else if(character == '.'){
			//na konci vety musi byt prave 3 lomitka
			if(previous.compare("/") == 0){
				cout << "//";
				fputs("//", outfile);
				previous += "/";
			}
			//pokud byla na konci textu mezera, tak se pripise na konec 
			//jen jedno lomitko
			else if(previous.compare("//") == 0){
				cout << "/";
				fputs("/", outfile);
				previous += "/";
			}
			
		}
		//jine interpunkce nez tecka a "bily znaky" ktery nejsou mezera 
		//nebo konec radku se vynechavaji
		else if(!ispunct(character) && !isspace(character)){
			cout << "...Text nelze prevezt do morseovy abecedy..." << endl;
			return 1;
		}
		numberAscii = fgetc(infile);
	}while(numberAscii != EOF);
	return 0;
}

//funkce pro prepis z morseovky do textu
bool prepis1(FILE * infile,FILE * outfile){
	int numberAscii;
	char character;
	//kvuli rozhodnuti, jestli napsat mezeru ci konec vety
	string previous;
	previous.clear();
	//priznak velkeho pismena na konci vety
	bool big = 1;
	//pro urychleni programu
	char print = NULL;
	string morse;
	numberAscii = fgetc(infile); //osetrit prazdny soubor
	do{
		character = (char) numberAscii;
		while(character != '/'){
			if((character == '-')||(character == '.')){
				previous.clear();
				morse += character;
			}
			else if(!isspace(character)){ //bile znaky se ignoruji
				cout << "...Morseovku nelze prevezt do textu..." << endl;
				return 1;
			}
			numberAscii = fgetc(infile);
			character = (char) numberAscii;
		}

		//zapisuje se jen kdyz je co
		if((previous.compare("") == 0)&&(!morse.empty())){
			if(big){ //pokud je priznak big, pak pismeno ma byt velke
				print = toString(morse);
				big = 0;
			}
			else{
				print = (char) tolower(toString(morse));
			}
			cout << print;
			fputc(print, outfile);
		}
		previous += "/";

		if(previous.compare("//") == 0){
			numberAscii = fgetc(infile);
			character = (char) numberAscii;
			//pokud jsou 3 lomitka, pak je to konec vety a ne mezera
			if(character == '/'){
				cout << ". ";
				fputs(". ", outfile);
				//na konci vety bude velke pismeno
				big = 1;
				numberAscii = fgetc(infile);
				character = (char) numberAscii;
			}
			//2 lomitka znaci konec slova
			else{
				cout << " ";
				fputs(" ", outfile);
			}
			
		}

		//nacte se dalsi znak
		else{
			numberAscii = fgetc(infile);
			character = (char) numberAscii;
		}

		//pokud se nacetl novy radek, pak se odradkuje. Prazdny radky se ignoruji
		if(numberAscii == 10){
			if(previous.compare("\n") != 0){
				cout << endl;
				fputs("\n", outfile);
			}
			previous = "\n";
		}
		morse.clear();
	}while(numberAscii != EOF);
	return 0;
}

//funkce pro otevreni souboru pro prepis z textu (morseovky) do morseovky (textu)
bool textMorse(bool mode){
	bool end = 0;
	//funkce pro otevreni vstupniho souboru
	FILE * infile = openFile("vstup");
	
	if(infile == NULL){
		return 1;
	}

	//funkce pro nacteni vystupniho souboru
	FILE * outfile = openFile("vystup");
	if(outfile == NULL){
		//zavreni vstupniho souboru
		fclose(infile);
		return 1;
	}

	if(mode){
		//volani funkce pro prepis z morseovky do textu
		if(prepis1(infile,outfile)){
			//priznak chyby
			end = 1;	
		}
	}

	else{
		//volani funkce pro prepis z textu do morseovky
		if(prepis0(infile,outfile)){
			//priznak chyby
			end = 1;	
		}
	}

	//zavreni vstupniho souboru
	fclose(infile);

	//zavreni vystupniho souboru
	fclose(outfile);
	return end;
}

int main(){
	//inicializace promenne znacici jaka operace se bude vykonavat
	string operation;
	//cout << "Zadejte cislo operace, nebo pro napovedu zadejte tlacitko \"h\"" << endl;
	cout << "Program pro prepis textoveho souboru do morseovky a naopak" << endl << endl;
	cout << "----------------------------------------------------" << endl;
	cout << "z textoveho souboru do morseovky zadej 0," << endl;
	cout << "z morseovky do textoveho souboru zadej 1," << endl;
	cout << "pro napovedu zadej tlacitko \"h\", pro konec \"k\":" << endl;
	cout << "----------------------------------------------------" << endl;
	cout << "Zadano:";
	//nekonecny cyklus - konci pri zvoleni spravne operace
	do{
		// nacteni znaku od uzivatele
		cin >> operation;
		cout << "----------------------------------------------------" << endl << endl;
		//rozpoznani operace
		if(operation.compare("h") == 0){
			help();
		}
		else if(operation.compare("0") == 0){
			//cout << "z textoveho souboru do morseovky" << endl;
			if(textMorse(0)){
				cout << "Chyba pri prepisu z textoveho souboru do morseovky";
			}
		}
		else if(operation.compare("1") == 0){
			//cout << "z morseovky do textoveho souboru" << endl;
			if(textMorse(1)){
				cout << "Chyba pri prepisu z morseovky do textoveho souboru";
			}
		}
		else if(operation.compare("k") == 0){
			return 0;
		}
		else{
			cout << "Takovou operaci program nenabizi" << endl;
			
		}
		
		cout << endl << "====================================================" << endl;
		cout << "...novypokus...:";
	}while(1);

	//sem se program nedostane - ano vim ze pouzivam nekonecny cyklus, 
	//ale je dobre osetren a lze z nej vpoho vyskocit :-)
	/*system("PAUSE");
	return 0;*/
}
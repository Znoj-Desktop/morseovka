import java.util.Scanner;
import java.io.*;
//vse v jednom souboru - rekl bych typicka ukazka jak to nema vypadat :-D
public class morse{
	static String [] alpha = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-",
			".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
			"-.--", "--.."};
	static String [] number = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};
	//tabulka pro prevod diakritiky
	static int [] iso8859_2 = {83, 0, 83, 84, 90, 90, 0, 65, 0, 0, 0, 76, 83, 0, 0, 83, 83, 84, 90, 0, 
		90, 90, 0, 65, 0, 0, 0, 76, 83, 0, 0, 83, 83, 84, 90, 0, 90, 90, 82, 65, 65, 65, 65, 76, 67, 67,
		67, 69, 69, 69, 69, 73, 73, 68, 68, 78, 78, 79, 79, 79, 79, 0, 82, 85, 85, 85, 85, 89, 84, 0, 82,
		65, 65, 65, 65, 76, 67, 67, 67, 69, 69, 69, 69, 73, 73, 68, 68, 78, 78, 79, 79, 79, 79, 0, 82, 85,
		85, 85, 85, 89, 84, 0};
	
	//vypis nabidky programu
	public static void offer(String args[]){ 
		System.out.println ("h - napoveda, 0 - z morseovky do textu, 1 - z textu do morseovky, k - konec"); 	
		System.out.print("zadej operaci: ");
	}
  
	//vydynda data od uzivatele a vraci je jako svoji navratovou hodnotu
	public static String read(){
		Scanner sc = new Scanner(System.in);
		return sc.next();
	}
  
	//otevreni vstupniho souboru vcetne dialogu a osetreni
	public static InputStream openInFile(){
		System.out.println("zadej n�zev vstupniho souboru i s cestou");
		String inputName = read();
		InputStream input = null;
		try {
			input = new FileInputStream(inputName);
		} catch (IOException e){
			System.out.println("Soubor "+inputName+" se nepodarilo otevrit!"); 
			return null;  
		}
		return input;
	}
  
	//otevreni vystupniho souboru vcetne dialogu a osetreni
	public static OutputStream openOutFile(){
		System.out.println("zadej n�zev vystupniho souboru i s cestou");
		String outputName = read();
		OutputStream output = null;
		try {
			output = new FileOutputStream(outputName);
		} catch (IOException e){
			System.out.println("Soubor "+outputName+" se nepodarilo otevrit!");
			return null;  
		}
		return output;
  }
  
	//otevreni souboru a volani prislusne funkce podle typu operace
	public static void operation(int type) throws IOException{
		InputStream input = openInFile();
		OutputStream output = openOutFile();
		if(type == 0){
			morseToText(input, output);
		}
		else if(type == 1){
			textToMorse(input, output);
		}
	}

	//sekvencni vyhledani znaku morseovy abecedy v tabulce
	public static char toString(String morse){
		if(morse.isEmpty()){
			;
		}

		//cislo
		if(morse.length() == 5){
			for(int i=0; i<10; i++){
				if(morse.compareTo(number[i]) == 0){
					return (char) (i+48);
				}
			}
			//System.out.println("Znak nepatri do morseovy abecedy");
			//return (Character) null;
		}
		//pismena
		else{
			for(int i=0; i<26; i++){
				if(morse.compareTo(alpha[i]) == 0){
					return (char) (i+65);
				}
			}
			
		}
		return 0;
	}
	
	public static void morseToText(InputStream input, OutputStream output) throws IOException{
		int asciiNumber; //nacteny znak jako cislo
		char character; //znak pro zapis
		String previous = new String(); //priznak pro vypisovani mezer
		Boolean big = true; //priznak velkeho pismena na konci vety
		char print; //urychluje program
		String morse = new String(); //ukladani nacitanych sekvenci znaku morseovy abecedy
		asciiNumber = input.read(); //nacteni znaku
		PrintStream ps = new PrintStream(output);; //pro zapis do souboru
		do{
			character = (char) asciiNumber;
			while(character != '/'){
				if((character == '.') || (character == '-')){
					previous = "";
					morse += character;
				}
				else if((character != ' ') || (character != '\n') || (character != '\r')){
					; //mezery se ignoruji
				}
				else{
					System.out.println("soubor obsahuje znaky, ktere do morseovky nepatri");
					System.exit(1);
				}
				asciiNumber = input.read();
				character = (char) asciiNumber;
			} //konec slova
			
			// zapis - jen toho co je treba
			if((previous.equals(""))&&(!morse.equals(null))){
				print = toString(morse);
				if(big){ //pismeno ma byt velke
					big = false;
				}
				else{
					print+= 32;
				}
				ps.print(print);
			}
			previous += "/";
			
			//jedna se o konec slova nebo cele vety
			if(previous.compareTo("//") == 0){
				asciiNumber = input.read();
				character = (char) asciiNumber;
				//pokud jsou 3 lomitky, pak se jedna o konec vety
				if(character == '/'){
					ps.print(". ");
					big = true; //na zacatku nove vety velke pismeno
					asciiNumber = input.read();
					character = (char) asciiNumber;
				}
				//pokud jsou lomitka prave 2, pak se jedna o konec slova
				else{
					ps.print(" ");
				}
			}
			//nacte se dalsi znak
			else{
				asciiNumber = input.read();
				character = (char) asciiNumber;
			}	
			//pokud se nacte znak konce radku, pak je odradkovano
			if((character == '\n') || (character == '\r')){
				if(previous.compareTo("\n") != 0){
					ps.print("\n");
				}
				previous = "\n";
			}
			morse = new String();
		}while(asciiNumber != -1);

	}

	public static void textToMorse(InputStream input, OutputStream output) throws IOException{
		PrintStream ps = null;
		Boolean whiteCharacter = false; //priznak mezery
		Boolean sentence = false; //priznak znaku konce vety
		ps = new PrintStream(output);
		while(input.available() != 0){
			int asciiNumber = input.read(); //nacteni znaku
			
			//prevod z diakritiky dle normy iso8859-2, castecne kompatibilni s CP 1250
			if(asciiNumber>153){
				asciiNumber = iso8859_2[asciiNumber-154];
			}
			
			//prevod na znaky morseovy abecedy
			if((asciiNumber == 33) || (asciiNumber == 46) || (asciiNumber == 63)){ //pokud konec vety
				if(!sentence){
					if(whiteCharacter){
						ps.print("/");
					}
					else{
						ps.print("//");
					}
					sentence = true;
					whiteCharacter = true;
				}
			}
			else if((asciiNumber <= 48) || ((asciiNumber > 57) &&(asciiNumber < 64)) || ((asciiNumber > 90) &&(asciiNumber < 97))|| ((asciiNumber > 122) &&(asciiNumber < 127))){ //pokud mezera, nebo neco co muze byt konec slova
				if(!whiteCharacter){
					ps.print("/");
					whiteCharacter = true;
				}
				sentence = false;
			}
			else {
				//cisla
				if(asciiNumber < 58){
					ps.print(number[asciiNumber-48] + "/");
				}
				//velka pismena
				else if(asciiNumber < 91){
					ps.print(alpha[asciiNumber-65] + "/");
				}
				//mala pismena
				else if(asciiNumber < 123){
					ps.print(alpha[asciiNumber-97] + "/");
				}
				
				whiteCharacter = false;
				sentence = false;
			}
		}
		ps.close();
	}
 
	public static void main(String[] args) throws IOException{
		//vyzva k zadani operace
	  	offer(args); 
	  	//ukladani ziskaneho retezce do promenne operation
	  	String operation = read();
	  	if(operation.compareTo("h") == 0){
	  		System.out.println("napoveda");
	  	}
	  	else if(operation.compareTo("0") == 0){
	  		System.out.println("z morseovky do textu...");
	  		operation(0);
	  		System.out.println("hotovo...");
	  	}
	  	else if(operation.compareTo("1") == 0){
	  		System.out.println("z textu do morseovky...");
	  		operation(1);
	  		System.out.println("hotovo...");
	  	}
	  	else if(operation.compareTo("k") == 0){
	  		System.out.println("konec");
	  	}
	  	else {
	  		System.out.println("chyba, neznama operace!");
	  	}
	  	System.out.print("___________________________________");
	  }
	}